import { Component, OnInit } from '@angular/core';
import { UserService } from '../UserService';
import { User } from '../User';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  userType : string;
  userName : string;
  results : User[] = [];
  selected : boolean = false;
  selectedUser : User;
  searchUserType : string = '';
  searchUserName : string = '';


  constructor(private userService : UserService) { }

  ngOnInit() {
  }

  handleChange(){
    this.search(this.searchUserType, this.searchUserName);
    console.log(this.results);
  }

  search(type : string, name : string){
    this.selected = false;
    this.userType = type;
    this.userName = name;
    this.userService.getUsersByNameAndType(this.userName, this.userType).subscribe(
      users => {
        this.results = users;
      },
      error => {
        this.results = []
      }
    )
  }
}
