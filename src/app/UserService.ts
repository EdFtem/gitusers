import { Injectable } from '@angular/core';
import { Http } from '@angular/http'
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './User';

@Injectable()
export class UserService{
    private SearchUsersUrl = "https://api.github.com/search/users?q=";
    private GetUserDetailsAndPoint = "https://api.github.com/users";
    
    constructor(private http: Http){
        
    }

    getUsersByNameAndType(name: string, userType: string){
        let url;
        if(name && !userType){
            url = `${this.SearchUsersUrl}${name} in:login`;
        }else if(!name && userType){
            url = `${this.SearchUsersUrl}type:${userType}`;
        }else if (name && userType){
            url = `${this.SearchUsersUrl}type:${userType} ${name} in:login`;
        }else {
            url = this.GetUserDetailsAndPoint;
        }
        this.http.get(url);

        return this.http.get(url).pipe(map(res => <User[]>res.json().items));
    }

    getDetailsByUserName(username : string){
        if(username){
            let url = `${this.GetUserDetailsAndPoint}/${username}`;
            return this.http.get(url)
            .pipe(map(res => <User>res.json()));
        }
    }
}